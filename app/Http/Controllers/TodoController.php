<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function __construct() {
        // $this->middleware( 'auth' , ['only' => [
        //     'create', 'update', 'delete'
        // ]]);
        $this->middleware('auth');
    }

    public function showAllTodos()
    {
        return response()->json(Todo::all());
    }

    public function showOneTodo($id)
    {
        return response()->json(Todo::find($id));
    }

    public function create(Request $request)
    {
        $todo = Todo::create($request->all());

        return response()->json($todo, 201);
    }

    public function update($id, Request $request)
    {
        $todo = Todo::findOrFail($id);
        $todo->update($request->all());

        return response()->json($todo, 200);
    }

    public function delete($id)
    {
        Todo::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
